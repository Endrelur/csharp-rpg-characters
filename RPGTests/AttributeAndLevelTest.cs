using ConsoleRPG.Characters;
using ConsoleRPG.Characters.Classes;

namespace RPGTests
{
    public class AttributeAndLevelTest
    {

        [Fact]
        public void TotalAttributes_WarriorLevelTwo_ProperTotalAttributesForWarriorLevelTwo()
        {
            Character testCharacter = new Warrior("Barac");


            testCharacter.LevelUp();
            Attributes actual = testCharacter.TotalAttributes();
            //primary + one level scalar
            int expectedStrength = 5 + 3;
            int expectedDexterity = 2 + 2;
            int expectedIntelligence = 1 + 1;
            Attributes expected = new Attributes(expectedStrength, expectedDexterity, expectedIntelligence);

            //Attributes should be similar
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttributes_RangerLevelTwo_ProperTotalAttributesForRangerLevelTwo()
        {
            Character testCharacter = new Ranger("Sanic");


            testCharacter.LevelUp();
            Attributes actual = testCharacter.TotalAttributes();
            //primary + one level scalar
            int expectedStrength = 1 + 1;
            int expectedDexterity = 7 + 5;
            int expectedIntelligence = 1 + 1;
            Attributes expected = new Attributes(expectedStrength, expectedDexterity, expectedIntelligence);

            //Attributes should be similar
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttributes_RogueLevelTwo_ProperTotalAttributesForRogueLevelTwo()
        {
            Character testCharacter = new Rogue("Obonga");


            testCharacter.LevelUp();
            Attributes actual = testCharacter.TotalAttributes();
            //primary + one level scalar
            int expectedStrength = 2 + 1;
            int expectedDexterity = 6 + 4;
            int expectedIntelligence = 1 + 1;
            Attributes expected = new Attributes(expectedStrength, expectedDexterity, expectedIntelligence);

            //Attributes should be similar
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttributes_MageLevelTwo_ProperTotalAttributesForMageLevelTwo()
        {
            Character testCharacter = new Mage("Zyzz");


            testCharacter.LevelUp();
            Attributes actual = testCharacter.TotalAttributes();
            //primary + one level scalar
            int expectedStrength = 1 + 1;
            int expectedDexterity = 1 + 1;
            int expectedIntelligence = 8 + 5;
            Attributes expected = new Attributes(expectedStrength, expectedDexterity, expectedIntelligence);

            //Attributes should be similar
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PrimaryAttributes_NewWarriorIsCreated_ProperDefaultAttributesForWarrior()
        {
            Character testCharacter = new Warrior("Barac");

            Attributes expected = new Attributes(5, 2, 1);
            Attributes actual = testCharacter.GetPrimaryAttributes();

            //Attributes should be similar
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PrimaryAttributes_NewRangerIsCreated_ProperDefaultAttributesForRanger()
        {
            Character testCharacter = new Ranger("Sanic");

            Attributes expected = new Attributes(1, 7, 1);
            Attributes actual = testCharacter.GetPrimaryAttributes();

            //Attributes should be similar
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PrimaryAttributes_NewRogueIsCreated_ProperDefaultAttributesForRogue()
        {
            Character testCharacter = new Rogue("Obonga");

            Attributes expected = new Attributes(2, 6, 1);
            Attributes actual = testCharacter.GetPrimaryAttributes();

            //Attributes should be similar
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PrimaryAttributes_NewMageIsCreated_ProperDefaultAttributesForMage()
        {
            Character testCharacter = new Mage("Zyzz");

            Attributes expected = new Attributes(1, 1, 8);
            Attributes actual = testCharacter.GetPrimaryAttributes();

            //Attributes should be similar
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Level_NewCharacterIsCreated_LeveIsOne()
        {
            Character testCharacter = new Warrior("Barac");

            var level = testCharacter.Level;

            //character level should be 1 when created.
            Assert.Equal(1, level);
        }

        [Fact]
        public void LevelUp_NewCharacterLevelUp_CharacterIsLevelTwo()
        {
            Character testCharacter = new Mage("Barac");

            var level = testCharacter.LevelUp();

            //character level should be 2 after first level up.
            Assert.Equal(2, level);

        }
    }
}