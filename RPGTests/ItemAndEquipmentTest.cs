﻿using ConsoleRPG.Characters;
using ConsoleRPG.Characters.Classes;
using ConsoleRPG.Exceptions;
using ConsoleRPG.Items;
using ConsoleRPG.Items.Armour;
using ConsoleRPG.Items.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace RPGTests
{
    public class ItemAndEquipmentTest
    {
        Weapon testAxe = new("Common axe", 1, Weapon.Type.Axe, 7, 1.1);
        Weapon testBow = new("Common bow", 1, Weapon.Type.Bow, 12, 0.8);

        Armor testPlateBody = new
            ("Common plate body armor",
            1,
            Item.Slot.Body,
            Armor.Type.Plate,
            new Attributes(1, 0, 0));

        Armor testClothHead = new
            ("Common cloth head armor",
            1,
            Item.Slot.Head,
            Armor.Type.Cloth,
            new Attributes(0, 0, 5));

        [Fact]
        public void Equip_WarriorLevelOneAxeLevelTwo_ThrowsInvalidWeaponException()
        {
            Character testWarrior = new Warrior("Barac");
            Weapon axeLevelTwo = testAxe;
            axeLevelTwo.LevelRequirement = 2;
            string expectedMessage = "Character level does not meet weapon level requirement. Level: 1 Required Level: 2";

            //Fails if a InvalidWeaponTypeException is not thrown.
            var e = Assert.Throws<InvalidWeaponException>(() =>
            {
                testWarrior.Equip(axeLevelTwo);
            });

            //Fails if the message differs
            Assert.Equal(expectedMessage, e.Message);
        }

        [Fact]
        public void Equip_WarriorAndBow_ThrowsInvalidWeaponException()
        {
            Character testWarrior = new Warrior("Barac");
            string expectedMessage = "Bow cannot be be equipped, wrong weapon type.";

            //Fails if a InvalidWeaponTypeException is not thrown.
            var e = Assert.Throws<InvalidWeaponException>(() =>
            {
                testWarrior.Equip(testBow);
            });

            //Fails if the message differs
            Assert.Equal(expectedMessage, e.Message);
        }

        [Fact]
        public void Equip_WarriorLevelOnePlateBodyLevelTwo_ThrowsInvalidArmorException()
        {
            Character testWarrior = new Warrior("Barac");
            Armor plateBodyLevelTwo = testPlateBody;
            plateBodyLevelTwo.LevelRequirement = 2;
            string expectedMessage = "Character level does not meet armor level requirement. Level: 1 Required Level: 2";

            //Fails if a InvalidArmorTypeException is not thrown.
            var e = Assert.Throws<InvalidArmorException>(() =>
            {
                testWarrior.Equip(plateBodyLevelTwo);
            });

            //Fails if the message differs
            Assert.Equal(expectedMessage, e.Message);
        }

        [Fact]
        public void Equip_WarriorAndClothHead_ThrowsInvalidArmorException()
        {
            Character testWarrior = new Warrior("Barac");
            string expectedMessage = "Cloth cannot be be equipped, wrong armor type.";

            //Fails if a InvalidArmorTypeException is not thrown.
            var e = Assert.Throws<InvalidArmorException>(() =>
            {
                testWarrior.Equip(testClothHead);
            });

            //Fails if the message differs
            Assert.Equal(expectedMessage, e.Message);
        }

        [Fact]
        public void Equip_WarriorLevelOneAndPlatebodyLevelOne_ReturnsSuccessMessage()
        {
            Character testWarrior = new Warrior("Barac");
            string expectedMessage = "New armor equipped!";

            //Equip platebody
            var actualMessage = testWarrior.Equip(testPlateBody);

            //Message should not differ.
            Assert.Equal(expectedMessage, actualMessage);
        }

        [Fact]
        public void Equip_WarriorLevelOneAndAxeLevelOne_ReturnsSuccessMessage()
        {
            Character testWarrior = new Warrior("Barac");
            string expectedMessage = "New weapon equipped!";

            //Equip platebody
            var actualMessage = testWarrior.Equip(testAxe);

            //Message should not differ.
            Assert.Equal(expectedMessage, actualMessage);
        }

        [Fact]
        public void Damage_WarriorLevelOneWithNoWeaponAndArmorEquipped_BaseDamage()
        {
            Character testWarrior = new Warrior("Barac");
            int expectedDamage = 1 * (1 + (5 / 100));

            var actualDamage = testWarrior.Damage();

            //Assert that the damage equals baseDamage.
            Assert.Equal(expectedDamage, actualDamage);
        }

        [Fact]
        public void Damage_WarriorLevelOneWithAxeAndNoArmorEquipped_AxeDpsPlusBaseDamage()
        {
            Character testWarrior = new Warrior("Barac");
            double expectedDamage = (7 * 1.1) * (1 + (5 / 100));

            var equipMessage = testWarrior.Equip(testAxe);
            var actualDamage = testWarrior.Damage();

            //Assert that the damage equals basedamage plus axe DPS.
            Assert.Equal(expectedDamage, actualDamage);
        }

        [Fact]
        public void Damage_WarriorLevelOneWithAxeAndPlateBodyArmorEquipped_AxeDpsPlusBaseDamagePlusArmourAttributes()
        {
            Character testWarrior = new Warrior("Barac");
            double expectedDamage = (7 * 1.1) * (1 + ((5 + 1) / 100));

            var equipMessage = testWarrior.Equip(testAxe);
            var equipMessage2 = testWarrior.Equip(testPlateBody);
            var actualDamage = testWarrior.Damage();

            //Assert that the damage equals basedamage plus axe DPS.
            Assert.Equal(expectedDamage, actualDamage);
        }



    }
}
