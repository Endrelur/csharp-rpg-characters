﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Characters
{
    /// <summary>
    /// A structure representing all the available character or item attributes in consoleRPG
    /// </summary>
    public struct Attributes
    {
        public int Strength { get; }
        public int Dexterity { get; }
        public int Intelligence { get; }

        public Attributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Attributes))
                return false;

            bool result = true;

            Attributes attributes = (Attributes)obj;

            if (Strength != attributes.Strength)
            {
                result = false;
            }
            if (Dexterity != attributes.Dexterity)
            {
                result = false;
            }
            if (Intelligence != attributes.Intelligence)
            {
                result = false;
            }
            return result;

        }

        public override int GetHashCode()
        {
            return System.HashCode.Combine(Strength, Dexterity, Intelligence);
        }
    }
}
