﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleRPG.Exceptions;
using ConsoleRPG.Items;
using ConsoleRPG.Items.Armour;
using ConsoleRPG.Items.Weapons;

namespace ConsoleRPG.Characters
{
    /// <summary>
    /// Parent class that represents all Characters in ConsoleRPG. Should be extended by any Classes in ConsoleRPG.
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        private Attributes PrimaryAttributes;
        private Attributes AttributeScalars;
        public Dictionary<Item.Slot, Item> Equipment { get; set; }
        public List<Weapon.Type> WeaponTypes { get; }
        public List<Armor.Type> ArmorTypes { get; }

        /// <summary>
        /// Constructor for a new character level 1.
        /// </summary>
        /// <param name="primaryAttributes">The base primary attributes for a character.</param>
        /// <param name="attributeScalars">The scalars that multiplies along with character level and is added to the primary attributes.</param>
        /// <param name="name">The name of the Character.</param>
        /// <param name="weaponTypes">A list containing all the allowed weapon types for a Character.</param>
        /// <param name="armorTypes">A list containing all the allowed armor types for a Character.</param>
        protected Character(Attributes primaryAttributes, Attributes attributeScalars, String name, List<Weapon.Type> weaponTypes, List<Armor.Type> armorTypes)
        {
            Name = name;
            PrimaryAttributes = primaryAttributes;
            AttributeScalars = attributeScalars;
            Equipment = new Dictionary<Item.Slot, Item>();
            WeaponTypes = weaponTypes;
            ArmorTypes = armorTypes;
            Level = 1;
        }

        /// <summary>
        /// Levels up a character 1 level.
        /// </summary>
        /// <returns>The level that the character achieved after it leveled up.</returns>
        public int LevelUp()
        {
            Level++;
            return Level;
        }

        /// <summary>
        /// Getter for the primary or base attributes of the character.
        /// </summary>
        /// <returns>The primary or base attributes of the character.</returns>
        public Attributes GetPrimaryAttributes()
        {
            return PrimaryAttributes;
        }

        /// <summary>
        /// Getter for the total attributes of a character which includes base attributes + attributes gained from levels.
        /// </summary>
        /// <returns>The total attributes of a character.</returns>
        public Attributes TotalAttributes()
        {
            int strength = PrimaryAttributes.Strength + AttributeScalars.Strength * (Level - 1);
            int dexterity = PrimaryAttributes.Dexterity + AttributeScalars.Dexterity * (Level - 1);
            int intelligence = PrimaryAttributes.Intelligence + AttributeScalars.Intelligence * (Level - 1);
            foreach (Item item in Equipment.Values)
            {
                if (item is Armor)
                {
                    Armor armor = (Armor)item;
                    strength += armor.ArmorAttributes.Strength;
                    dexterity += armor.ArmorAttributes.Dexterity;
                    intelligence += armor.ArmorAttributes.Intelligence;
                }
            }
            return new Attributes(strength, dexterity, intelligence);
        }

        /// <summary>
        /// Produces a string containing the following stats for a single character: 
        /// Name, Level, Strength, Dexterity, Intelligence and Damage.
        /// </summary>
        /// <returns>A string containing all the stats of a single character.</returns>
        public string GetStatsString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Name: ").Append(Name).AppendLine();
            sb.Append("Level: ").Append(Level).AppendLine();
            Attributes attributes = TotalAttributes();
            sb.Append("Strength: ").Append(attributes.Strength).AppendLine();
            sb.Append("Dexterity: ").Append(attributes.Dexterity).AppendLine();
            sb.Append("Intelligence: ").Append(attributes.Intelligence).AppendLine();
            sb.Append("Damage: ").Append(Damage());
            return sb.ToString();
        }
        /// <summary>
        /// Gets the damage of the character which is affected by character, weapon and armor stats.
        /// </summary>
        /// <returns>Gets the total damage for a character.</returns>
        public abstract double Damage();

        /// <summary>
        /// Equips a item for 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// <exception cref="InvalidArmorException">
        /// Thrown if the level requirement is too high or the armor type is invalid.
        /// </exception>
        /// <exception cref="InvalidWeaponException">
        /// Thrown if the level requirement is too high or the weapon type is invalid.
        /// </exception>
        public string Equip(Item item)
        {
            switch (item.ItemSlot)
            {
                case Item.Slot.Weapon:
                    return EquipWeapon((Weapon)item);
                default:
                    return EquipArmor((Armor)item);

            }
        }

        private string EquipWeapon(Weapon weapon)
        {
            if (!WeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException(weapon.WeaponType + " cannot be be equipped, wrong weapon type.");
            }
            if (Level < weapon.LevelRequirement)
            {
                throw new InvalidWeaponException("Character level does not meet weapon level requirement. Level: "
                    + Level + " Required Level: " + weapon.LevelRequirement);
            }

            Equipment.Add(weapon.ItemSlot, weapon);
            return "New weapon equipped!";

        }

        private string EquipArmor(Armor armor)
        {
            if (!ArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException(armor.ArmorType + " cannot be be equipped, wrong armor type.");
            }
            if (Level < armor.LevelRequirement)
            {
                throw new InvalidArmorException("Character level does not meet armor level requirement. Level: "
                    + Level + " Required Level: " + armor.LevelRequirement);
            }

            Equipment.Add(armor.ItemSlot, armor);
            return "New armor equipped!";
        }
    }
}