﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.Characters;
using ConsoleRPG.Items;
using ConsoleRPG.Items.Armour;
using ConsoleRPG.Items.Weapons;

namespace ConsoleRPG.Characters.Classes
{
    /// <summary>
    /// A class representing the class of type rogue in the consoleRPG
    /// </summary>
    public class Rogue : Character
    {
        private static readonly Attributes primary = new Attributes(2, 6, 1);
        private static readonly Attributes scalars = new Attributes(1, 4, 1);
        private static readonly List<Weapon.Type> WeaponTypes = new List<Weapon.Type> { Weapon.Type.Dagger, Weapon.Type.Sword };
        private static readonly List<Armor.Type> ArmorTypes = new List<Armor.Type> { Armor.Type.Mail, Armor.Type.Leather };

        /// <summary>
        /// Constructor for a new rogue-class. Rogue scales primarily on dexterity, can wear leather, mail as armor and dagger, sword as weapon.
        /// </summary>
        /// <param name="name">The name of the new character of class rogue.</param>
        public Rogue(string name)
                    : base(primary, scalars, name, WeaponTypes, ArmorTypes)

        {

        }

        public override double Damage()
        {
            if (Equipment[Item.Slot.Weapon] is not Weapon)
            {
                return 1;
            }
            else
            {
                Weapon weapon = (Weapon)Equipment[Item.Slot.Weapon];
                return weapon.DPS() * (1 + TotalAttributes().Dexterity / 100);
            }
        }
    }
}
