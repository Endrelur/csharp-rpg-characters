﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG.Characters;
using ConsoleRPG.Items;
using ConsoleRPG.Items.Armour;
using ConsoleRPG.Items.Weapons;

namespace ConsoleRPG.Characters.Classes
{
    /// <summary>
    /// A class representing the class of type mage in the consoleRPG
    /// </summary>
    public class Mage : Character
    {
        private static readonly Attributes primary = new Attributes(1, 1, 8);
        private static readonly Attributes scalars = new Attributes(1, 1, 5);
        private static readonly List<Weapon.Type> WeaponTypes = new List<Weapon.Type> { Weapon.Type.Wand, Weapon.Type.Staff };
        private static readonly List<Armor.Type> ArmorTypes = new List<Armor.Type> { Armor.Type.Cloth };

        /// <summary>
        /// Constructor for a new mage-class. Mage scales primarily on damage, can wear cloth as armor and wands, staffs as weapon.
        /// </summary>
        /// <param name="name">The name of the new character of class mage.</param>
        public Mage(string name) : base(primary, scalars, name, WeaponTypes, ArmorTypes)

        {

        }

        public override double Damage()
        {
            if (Equipment[Item.Slot.Weapon] is not Weapon)
            {
                return 1;
            }
            else
            {
                Weapon weapon = (Weapon)Equipment[Item.Slot.Weapon];
                return weapon.DPS() * (1 + TotalAttributes().Intelligence / 100);
            }
        }
    }
}
