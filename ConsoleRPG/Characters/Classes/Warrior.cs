﻿using ConsoleRPG.Items;
using ConsoleRPG.Items.Armour;
using ConsoleRPG.Items.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Characters.Classes
{
    /// <summary>
    /// A class representing the class of type warrior in the consoleRPG
    /// </summary>
    public class Warrior : Character
    {

        private static readonly Attributes primary = new Attributes(5, 2, 1);
        private static readonly Attributes scalars = new Attributes(3, 2, 1);
        private static readonly List<Weapon.Type> WeaponTypes = new List<Weapon.Type> { Weapon.Type.Axe, Weapon.Type.Hammer, Weapon.Type.Sword };
        private static readonly List<Armor.Type> ArmorTypes = new List<Armor.Type> { Armor.Type.Mail, Armor.Type.Plate };

        /// <summary>
        /// Constructor for a new warrior-class. Warrior scales primarily on strength, can wear plate, mail as armor and axe, sword, hammer as weapon.
        /// </summary>
        /// <param name="name">The name of the new character of class warrior.</param>
        public Warrior(string name)
            : base(primary, scalars, name, WeaponTypes, ArmorTypes)
        {
        }

        public override double Damage()
        {
            double baseDamage = (1 + TotalAttributes().Strength / 100);
            try
            {
                if (Equipment[Item.Slot.Weapon] is not Weapon)
                {
                    return baseDamage;
                }
                else
                {
                    Weapon weapon = (Weapon)Equipment[Item.Slot.Weapon];
                    return weapon.DPS() * baseDamage;
                }
            }
            catch (KeyNotFoundException knfe) //the Character has never equipped a weapon
            {
                return baseDamage;
            }


        }
    }
}
