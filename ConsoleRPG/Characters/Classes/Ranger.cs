﻿using ConsoleRPG.Items;
using ConsoleRPG.Items.Armour;
using ConsoleRPG.Items.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Characters.Classes
{
    /// <summary>
    /// A class representing the class of type ranger in the consoleRPG
    /// </summary>
    public class Ranger : Character
    {
        private static readonly Attributes primary = new Attributes(1, 7, 1);
        private static readonly Attributes scalars = new Attributes(1, 5, 1);
        private static readonly List<Weapon.Type> WeaponTypes = new List<Weapon.Type> { Weapon.Type.Bow };
        private static readonly List<Armor.Type> ArmorTypes = new List<Armor.Type> { Armor.Type.Mail, Armor.Type.Leather };

        /// <summary>
        /// Constructor for a new ranger-class. Ranger scales primarily on dexterity, can wear leather, mail as armor and bows as weapon.
        /// </summary>
        /// <param name="name">The name of the new character of class ranger.</param>
        public Ranger(string name) : base(primary, scalars, name, WeaponTypes, ArmorTypes)
        {
        }

        public override double Damage()
        {
            if (Equipment[Item.Slot.Weapon] is not Weapon)
            {
                return 1;
            }
            else
            {
                Weapon weapon = (Weapon)Equipment[Item.Slot.Weapon];
                return weapon.DPS() * (1 + TotalAttributes().Dexterity / 100);
            }
        }
    }
}
