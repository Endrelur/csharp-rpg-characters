﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Items.Weapons
{
    /// <summary>
    /// Class representing a single damage dealing weapon in the ConsoleRPG scope.
    /// </summary>
    public class Weapon : Item
    {
        public int BaseDamage { get; set; }
        public double Attackspeed { get; set; }
        public Type WeaponType { get; }

        /// <summary>
        /// Constructor for a new weapon.
        /// </summary>
        /// <param name="name">The name of the weapon.</param>
        /// <param name="levelRequirement">The character level required for equipping the weapon.</param>
        /// <param name="weaponType">The type of weapon, can be: Axe, Bow, Dagger, Hammer, Staff, Sword, Wand</param>
        /// <param name="baseDamage">The base damage of the weapon.</param>
        /// <param name="attackSpeed">The attack speed of the weapon measured in attacks/seconds.</param>
        public Weapon(string name, int levelRequirement, Type weaponType, int baseDamage, double attackSpeed) : base(name, levelRequirement, Item.Slot.Weapon)
        {
            WeaponType = weaponType;
            BaseDamage = baseDamage;
            Attackspeed = attackSpeed;
        }

        /// <summary>
        /// Getter for the DPS or Damage Per Second this weapon does.
        /// </summary>
        /// <returns>The DPS or Damage Per Second this weapon does.</returns>
        public double DPS()
        {
            return BaseDamage * Attackspeed;
        }

        /// <summary>
        /// Enumerative defining the different weapon types weapons can consist of.
        /// </summary>
        public enum Type
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand,
        }

    }


}
