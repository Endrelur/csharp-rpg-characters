﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Items
{
    /// <summary>
    /// A class defining what a item should be in the ConsoleRPG-scope.
    /// A item as of now is typically something that can be used or worn by a character.
    /// </summary>
    public abstract class Item
    {
        public string Name { get; set; }
        public int LevelRequirement { get; set; }
        public Slot ItemSlot { get; set; }

        /// <summary>
        /// Constructor for a new item.
        /// </summary>
        /// <param name="name"> The name of the item.</param>
        /// <param name="levelRequirement"> The required character level for the item to be used.</param>
        /// <param name="itemSlot"> The slot the item slot should be in.</param>
        public Item(string name, int levelRequirement, Slot itemSlot)
        {
            Name = name;
            LevelRequirement = levelRequirement;
            ItemSlot = itemSlot;
        }

        /// <summary>
        /// Enumerative representing what slots an item should be able to be worn in.
        /// </summary>
        public enum Slot
        {
            Head,
            Body,
            Legs,
            Weapon,
        }
    }
}
