﻿using ConsoleRPG.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Items.Armour
{
    /// <summary>
    /// Class representing a single armor-piece in the ConsoleRPG scope.
    /// </summary>
    public class Armor : Item
    {
        public Attributes ArmorAttributes { get; set; }
        public Type ArmorType { get; set; }

        /// <summary>
        /// Constructor for a new armor-piece.
        /// </summary>
        /// <param name="name">The name of the armor piece.</param>
        /// <param name="levelRequirement">The character level that is required to wear the armor.</param>
        /// <param name="slot">The item slot that the piece is worn in.</param>
        /// <param name="type">The type of item, can be cloth, leather, mail or plate.</param>
        /// <param name="attributes">The character attributes that the item empowers when used.</param>
        public Armor(string name, int levelRequirement, Slot slot, Type type, Attributes attributes) : base(name, levelRequirement, slot)
        {
            ArmorType = type;
            ArmorAttributes = attributes;
        }

        /// <summary>
        /// Armor types.
        /// </summary>
        public enum Type
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
    }
}
