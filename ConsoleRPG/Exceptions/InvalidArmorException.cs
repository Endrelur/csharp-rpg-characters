﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Exceptions
{
    /// <summary>
    /// A exception that is thrown when there is a problem in the armor context of ConsoleRPG.
    /// This can for example be when a too low level character is trying to wear higher level armor.
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() { }
        public InvalidArmorException(string message, Exception inner) : base(message, inner) { }
        public InvalidArmorException(string message) : base(message) { }
    }
}
