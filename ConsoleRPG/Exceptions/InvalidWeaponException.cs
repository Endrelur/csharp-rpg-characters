﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG.Exceptions
{
    /// <summary>
    /// A exception that is thrown when there is a problem in the weapon context of ConsoleRPG.
    /// This can for example be when a too low level character is trying to wield a higher level weapon.
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() { }
        public InvalidWeaponException(string message, Exception inner) : base(message, inner) { }
        public InvalidWeaponException(string message) : base(message) { }
    }
}
